//
//  UIColor+HWExtension.m
//  HelpWill
//
//  Created by Viktor Peschenkov on 05.07.16.
//  Copyright © 2016 INOSTUDIO. All rights reserved.
//

@implementation UIColor (HWExtension)

+ (instancetype)hw_colorWithHexInt:(NSUInteger)hexColor {
    return [UIColor hw_colorWithHexInt:hexColor alpha:1.0f];
}

+ (instancetype)hw_colorWithHexInt:(NSUInteger)hexColor alpha:(CGFloat)alpha {
    return [UIColor colorWithRed:(float)HWFRGB((hexColor & 0xFF0000) >> 16)
                           green:(float)HWFRGB((hexColor & 0xFF00) >> 8)
                            blue:(float)HWFRGB((hexColor & 0xFF))
                           alpha:alpha];
}

+ (instancetype)hw_randomColor {
    CGFloat hue = ( arc4random() % 256 / 256.0 );
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;
    return [self colorWithHue:hue saturation:saturation brightness:brightness alpha:1.0f];
}

@end
